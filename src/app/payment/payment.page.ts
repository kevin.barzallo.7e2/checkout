import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  cardNumber:Number;
  cardName:Number;
  expMonth:Number;
  expYear:Number;
  cvv:Number;
  codigo_promocion:string;
  metodo_facturacion:string;
  checked:boolean;
  shoppingInfo;
  shippingMethod;


  constructor(private route: ActivatedRoute, private router: Router) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.shoppingInfo = this.router.getCurrentNavigation().extras.state.shopInfo;
        this.shippingMethod = this.router.getCurrentNavigation().extras.state.method;
      }  
    });

  }

  ngOnInit() {
  }

  finish(){
    if(this.checkInputs()){
      let paymentInfo = new PaymentInformation(this.cardNumber,this.cardName,this.expMonth,this.expYear,this.cvv,this.codigo_promocion,this.metodo_facturacion);
      this.passAll(paymentInfo);
    }
  }

  passAll(info:PaymentInformation){

    let navigationExtras: NavigationExtras = {
      state: {
        shopInfo: this.shoppingInfo,
        method: this.shippingMethod,
        paymentInfo: info,
      }
    };
    console.log(info);
    this.router.navigate(['info'], navigationExtras);

  }

  checkInputs():boolean{
    let inputs: Number[] = [this.cardNumber,this.cardName,this.expMonth,this.expYear,this.cvv];
    let inputVacio: boolean = false;
    inputs.forEach(input => {
      inputVacio = this.isEmpty(input);                  
    });
    
    if(inputVacio){
      return false;
    }else{
      return true;
    }
  }

  isEmpty(input:Number):boolean{
    if(input === undefined){
      return true;
    }else{ return false; }
  }


}

export class PaymentInformation{
  cardNumber:Number;
  cardName:string;
  expMonth:Number;
  expYear:Number;
  cvv:Number;
  codigo_promocion:string;
  metodo_facturacion:string;

  constructor(cardNumber,cardName,expMonth,expYear,cvv,codigo_promocion,metodo_facturacion){
    this.cardNumber=cardNumber;
    this.cardName=cardName;
    this.expMonth=expMonth;
    this.expYear=expYear;
    this.cvv=cvv;
    this.codigo_promocion=codigo_promocion;
    this.metodo_facturacion=metodo_facturacion;
  }

  getInfo(){
    
    let info = "Number Card: " + this.cardNumber + "\n Card Name: " + this.cardName + "\n Exp. Month: " + this.expMonth +
    "\n Exp. Year: " + this.expYear + "\n CVV: " + this.cvv + "\nMetodo Facturación: " + this.metodo_facturacion;
    return info;
  }

}
