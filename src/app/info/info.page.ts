import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { PaymentInformation } from '../payment/payment.page';
import { ShoppingInformation } from '../home/home.page';


@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  paymentInfo:PaymentInformation;
  infoPayment;
  infoShopping;
  method;

  constructor(private route: ActivatedRoute, private router: Router) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.paymentInfo = this.router.getCurrentNavigation().extras.state.paymentInfo;
        this.infoPayment = this.paymentInfo.getInfo();
        this.method = this.router.getCurrentNavigation().extras.state.method;
        let shopping:ShoppingInformation = this.router.getCurrentNavigation().extras.state.shopInfo;
        this.infoShopping = shopping.getInfo();
      }  
    });


  }
  ngOnInit() {
  }

}
