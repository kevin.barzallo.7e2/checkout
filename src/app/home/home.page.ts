import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  codigo_promocion:string;
  email:string;
  nombre:string;
  apellido:string;
  empresa:string;
  direccion1:string;
  direccion2:string;
  ciudad:string;
  codigo_postal:string;
  telefono:string;
  checked:boolean;
  pais:string;
  provincia: string;

  constructor(private router:Router) {}

  
  continue(){
    if(this.checkInputs()){
      let shoppingInformation = new ShoppingInformation(this.getCodigoPromocion(),this.getEmail(),this.getNombre(),this.getApellido(),this.getEmpresa(),this.getDireccion1(),this.getDireccion2(),this.getCiudad(),this.getCodigoPostal(),this.getTelefono(),this.getChecked(),this.getPais(),this.getProvincia())
      this.passShoppingInformation(shoppingInformation);
    }
  }

  passShoppingInformation(info:ShoppingInformation){
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: info,
      }
    };
    this.router.navigate(['envio'], navigationExtras);
  }

  checkInputs():boolean{
    let inputs: string[] = [this.getEmail(),this.getNombre(),this.getApellido(),this.getDireccion1(),this.getPais(),this.getCiudad(),this.getProvincia(),this.getCodigoPostal(),this.getTelefono()];
    let inputVacio: boolean = false;
    inputs.forEach(input => {
      inputVacio = this.isEmpty(input);                  
    });
    
    if(inputVacio){
      return false;
    }else{
      return true;
    }
  }

  isEmpty(input:string):boolean{
    if(input === "" || input === undefined){
      return true;
    }else{ return false; }
  }

  getCodigoPromocion(){
    return this.codigo_promocion;
  }

  getEmail(){
    return this.email;
  }

  getNombre(){
    return this.nombre;
  }

  getApellido(){
    return this.apellido;
  }

  getEmpresa(){
    return this.empresa;
  }

  getDireccion1(){
    return this.direccion1;
  }

  getDireccion2(){
    return this.direccion2;
  }

  getCiudad(){
    return this.ciudad;
  }

  getCodigoPostal(){
    return this.codigo_postal;
  }

  getTelefono(){
    return this.telefono;
  }

  getChecked(){
    return this.checked;

  }

  getPais(){
    return this.pais;

  }

  getProvincia(){
    return this.provincia;
  }
}

export class ShoppingInformation{

  codigo_promocion:string;
  email:string;
  nombre:string;
  apellido:string;
  empresa:string;
  direccion1:string;
  direccion2:string;
  ciudad:string;
  codigo_postal:string;
  telefono:string;
  checked:boolean;
  pais:string;
  provincia: string;

  constructor(codigo_promocion,email,nombre,apellido,empresa,direccion1,direccion2,ciudad,codigo_postal,telefono,checked,pais,provincia){

    this.codigo_postal = codigo_promocion;
    this.email = email;
    this.nombre = nombre;
    this.apellido = apellido;
    this.empresa = empresa;
    this.direccion1 = direccion1;
    this.direccion2 = direccion2;
    this.ciudad = ciudad;
    this.codigo_postal = codigo_postal;
    this.telefono = telefono;
    this.checked = checked;
    this.pais = pais;
    this.provincia = provincia;

  }

  getInfo(){
    let info = "Nombre: " + this.nombre + "\n Apellido: " + this.apellido + "\n Email: " + this.email +
    "\n Empresa: " + this.empresa + "\n Direccion 1: " + this.direccion1 + "\nDireccion 2: " + this.direccion2
    + "\n Pais: " + this.pais + "\n Ciudad: " + this.ciudad +
    "\n Provincia: " + this.provincia + "\n Codigo Postal: " + this.codigo_postal + "\nTelefono: " + this.telefono;
    return info;
  }

  getCodigoPromocion(){
    return this.codigo_promocion;
  }

  getEmail(){
    return this.email;
  }

  getNombre(){
    return this.nombre;
  }

  getApellido(){
    return this.apellido;
  }

  getEmpresa(){
    return this.empresa;
  }

  getDireccion1(){
    return this.direccion1;
  }

  getDireccion2(){
    return this.direccion2;
  }

  getCiudad(){
    return this.ciudad;
  }

  getCodigoPostal(){
    return this.codigo_postal;
  }

  getTelefono(){
    return this.telefono;
  }

  getChecked(){
    return this.checked;

  }

  getPais(){
    return this.pais;

  }

  getProvincia(){
    return this.provincia;
  }

}
