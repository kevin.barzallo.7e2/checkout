import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ShoppingInformation } from '../home/home.page';

@Component({
  selector: 'app-envio',
  templateUrl: './envio.page.html',
  styleUrls: ['./envio.page.scss'],
})
export class EnvioPage implements OnInit {

  data: ShoppingInformation;
  direccion1: string;
  direccion2: string;
  ciudad: string;
  codigo_postal: string;
  provincia: string;
  pais: string;
  telefono: string;
  empresa: string;
  metodo_envio: string;


  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        this.direccion1 = this.data.getDireccion1();
        this.ciudad = this.data.getCiudad();
        this.codigo_postal = this.data.getCodigoPostal();
        this.provincia = this.data.getProvincia();
        this.pais = this.data.getPais();
        this.telefono = this.data.getTelefono();
        this.direccion2 =this.data.getDireccion2();
        this.empresa= this.data.getEmpresa();
      }  
    });
  }

  continue(){
    if(this.getMetodo() != undefined){
      let navigationExtras: NavigationExtras = {
        state: {
          shopInfo: this.data,
          method: this.getMetodo(),
        }
      };
      this.router.navigate(['payment'], navigationExtras);
    }

  }

  getMetodo(){
    return this.metodo_envio;
  }

  ngOnInit() {
  }

}
